Sample application with Spring Boot 2 and Waffle for MS domain authentication.

Configured for Spring Basic filter (that is, not based in Waffle Basic filter) with waffle authentication manager.

Boot and browse http://localhost:8080/

By default, it allows ROLE_USER users (which is added by default by Waffle).
