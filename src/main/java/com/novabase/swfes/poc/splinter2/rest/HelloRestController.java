package com.novabase.swfes.poc.splinter2.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelloRestController.class);

  @GetMapping("/")
  @PreAuthorize("hasRole('ROLE_USER')")
  public String hello(Authentication auth) {
    LOGGER.debug("Authenticated as {} with roles {}", auth.getPrincipal(), auth.getAuthorities());
    return "Hello " + auth.getPrincipal() + " with roles " + auth.getAuthorities();
  }

}
