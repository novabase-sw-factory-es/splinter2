package com.novabase.swfes.poc.splinter2.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import waffle.spring.WindowsAuthenticationProvider;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

@Configuration
public class WaffleConfig {

  @Bean
  public WindowsAuthProviderImpl waffleWindowsAuthProvider() {
    return new WindowsAuthProviderImpl();
  }

  @Bean
  public WindowsAuthenticationProvider waffleSpringAuthenticationProvider(WindowsAuthProviderImpl windowsAuthProvider) {
    WindowsAuthenticationProvider windowsAuthenticationProvider = new WindowsAuthenticationProvider();
    windowsAuthenticationProvider.setAuthProvider(windowsAuthProvider);
    return windowsAuthenticationProvider;
  }

}
