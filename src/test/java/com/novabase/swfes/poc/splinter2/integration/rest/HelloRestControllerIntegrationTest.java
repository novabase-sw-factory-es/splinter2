package com.novabase.swfes.poc.splinter2.integration.rest;

import com.novabase.swfes.poc.splinter2.SpringBootApp;
import com.novabase.swfes.poc.splinter2.rest.HelloRestController;
import com.sun.security.auth.UserPrincipal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
public class HelloRestControllerIntegrationTest {

  private static final String USERNAME = "username";
  private static final String ROLE_USER = "ROLE_USER";
  private static final String ROLE_LUSER = "ROLE_LUSER";

  @Autowired
  private HelloRestController helloRestController;

  @Test
  @WithMockUser(username = USERNAME, authorities = {ROLE_USER})
  public void testUserGreeting() {
    String greeting = helloRestController.hello(user());
    String expectedGreeting = "Hello " + USERNAME + " with roles [" + ROLE_USER + "]";
    assertEquals(expectedGreeting, greeting);
  }

  @Test(expected = AccessDeniedException.class)
  @WithMockUser(username = USERNAME, authorities = {ROLE_LUSER})
  public void testLuserGreeting() {
    helloRestController.hello(luser());
  }

  private static Authentication user() {
    return userWithAuthority(ROLE_USER);
  }

  private static Authentication luser() {
    return userWithAuthority(ROLE_LUSER);
  }

  private static Authentication userWithAuthority(String authority) {
    Principal principal = new UserPrincipal(USERNAME);
    Collection<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(new SimpleGrantedAuthority(authority));
    return new UsernamePasswordAuthenticationToken(principal, "", authorities);
  }

}
